const getAllOrderMiddleware=(req,res,next)=>{
    console.log("GET all order middleware");
    next();
}

const createOrderMiddleware=(req,res,next)=>{
    console.log("POST order middleware");
    next();
}

const getOrderByIdMiddleware=(req,res,next)=>{
    console.log("Get detail order middleware");
    next();
}

const updateOrderMiddleware=(req,res,next)=>{
    console.log("Update order middleware");
    next();
}

const deleteOrderMiddleware=(req,res,next)=>{
    console.log("Delete order middleware");
    next();
}

module.exports={
    createOrderMiddleware,
    getAllOrderMiddleware,
    getOrderByIdMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}
