const getAllUserMiddleware=(req,res,next)=>{
    console.log("GET all user middleware");
    next();
}

const createUserMiddleware=(req,res,next)=>{
    console.log("POST user middleware");
    next();
}

const getUserByIdMiddleware=(req,res,next)=>{
    console.log("Get detail user middleware");
    next();
}

const updateUserMiddleware=(req,res,next)=>{
    console.log("Update user middleware");
    next();
}

const deleteUserMiddleware=(req,res,next)=>{
    console.log("Delete user middleware");
    next();
}

module.exports={
    createUserMiddleware,
    getAllUserMiddleware,
    getUserByIdMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}
