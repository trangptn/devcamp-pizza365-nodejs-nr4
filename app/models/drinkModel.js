const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const drinkSchema = new Schema({
   __id:mongoose.Types.ObjectId,
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia: {
        type: Number,
        required:true
    },
});

module.exports = mongoose.model("drink", drinkSchema);
