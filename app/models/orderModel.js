const mongoose = require("mongoose");

const Schema= mongoose.Schema;

const orderSchema= new Schema({
    __id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize:{
        type: String,
        required: true
    },
    pizzaType:{
        type:String,
        required: true
    },
    voucher:
        {
            type: String
        }
    ,
    drink:
        {
            type: String
        }
    ,
    status:{
        type:String,
        required:true
    }
});

module.exports=mongoose.model("order",orderSchema);