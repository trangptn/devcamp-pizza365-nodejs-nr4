const express= require("express");

const drinkRouter= express.Router();

const {createDrink, getAllDrinks,getDrinkById, updateDrinkById,deleteDrinkById}= require("../controllers/drinkController");

drinkRouter.post("/drinks", createDrink);
drinkRouter.get("/devcamp-pizza365/drinks",getAllDrinks);
drinkRouter.get("/drinks/:drinkid",getDrinkById);
drinkRouter.put("/drinks/:drinkid",updateDrinkById);
drinkRouter.delete("/drinks/:drinkid",deleteDrinkById);
module.exports= drinkRouter;