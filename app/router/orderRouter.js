const express=require('express');
const router=express.Router();

const {
    createOrderMiddleware,
    getAllOrderMiddleware,
    getOrderByIdMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}=require("../middlewares/orderMiddleware");

const{
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    orderHandle
}=require("../controllers/orderController");

router.post("/users/:userId/orders",createOrderMiddleware,createOrder);
router.get("/orders",getAllOrderMiddleware,getAllOrder);
router.get("/orders/:orderId",getOrderByIdMiddleware,getOrderById);
router.put("/orders/:orderId",updateOrderMiddleware,updateOrderById);
router.delete("/orders/:orderId",deleteOrderMiddleware,deleteOrderById);
router.post("/devcamp-pizza365/orders",orderHandle);
module.exports=router;