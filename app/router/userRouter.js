const express=require('express');
const router=express.Router();

const {
    createUserMiddleware,
    getAllUserMiddleware,
    getUserByIdMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}=require("../middlewares/userMiddleware");

const{
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
    getAllUserLimit,
    getAllUserSkip,
    getAllUserSorted,
    getAllUserSkipLimit,
    getAllUserSkipLimitSort
}=require("../controllers/userController");

router.post("/users",createUserMiddleware,createUser);
router.get("/users",getAllUserMiddleware,getAllUser);
router.get("/users/:userId",getUserByIdMiddleware,getUserById);
router.put("/users/:userId",updateUserMiddleware,updateUserById);
router.delete("/users/:userId",deleteUserMiddleware,deleteUserById);
router.get("/limit-users",getAllUserLimit);
router.get("/skip-users",getAllUserSkip);
router.get("/sort-users",getAllUserSorted);
router.get("/skip-limit-users",getAllUserSkipLimit);
router.get("/sort-skip-limit-users",getAllUserSkipLimitSort);
module.exports=router;