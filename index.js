//import thu vien express
const express = require('express');
const path = require('path');

const mongoose=require('mongoose');
const port= 8000;

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
.then(() =>{
    console.log("Connect mongoDB Successfully");
})
.catch((error) =>{
    console.log(error);
});

const app= new express();
app.use(express.json());

const userRouter=require("./app/router/userRouter");
const orderRouter=require("./app/router/orderRouter");
const drinkRouter=require("./app/router/drinkRouter");



app.use("/",userRouter);
app.use("/",orderRouter);
app.use("/",drinkRouter);

app.get("/",(req,res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/sample.06restAPI.order.pizza365.v2.0.html"))
})

app.listen(port, () => {
    console.log(`App  listening  on port ${port}`);
})